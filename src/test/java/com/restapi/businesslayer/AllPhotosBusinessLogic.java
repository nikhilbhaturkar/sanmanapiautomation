package com.restapi.businesslayer;

import static io.restassured.RestAssured.when;
import static io.restassured.path.json.JsonPath.from;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.restapi.datalayer.pojo.AllPhotos;
import com.restapi.datalayer.pojo.SinglePhoto;

import io.restassured.response.Response;

public class AllPhotosBusinessLogic {
	
	public AllPhotos getAllPhotosFor(String albumId) {
		String BaseUrl="";
		String AlbumId ="";
		String url = BaseUrl + AlbumId; 
		
		Response response = when().get(url);
		List<SinglePhoto> allSinglePhotos = Arrays.asList(response.getBody().as(SinglePhoto[].class));
		AllPhotos allphotos = new AllPhotos();
		allphotos.setAllPhoto(allSinglePhotos);
		return allphotos;
	}

}
