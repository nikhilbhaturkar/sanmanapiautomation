package com.restapi.testlayer;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import org.testng.annotations.Test;

public class SimpleRestApiTest {

	@Test
	public void validateStatusCode() {
		given().get("https://jsonplaceholder.typicode.com/users").then().statusCode(200).log().all();	
	}

	@Test
	public void testHasItemsFunction() {
		given().get("https://jsonplaceholder.typicode.com/users").then().statusCode(200).body("address.street",
				hasItem("Kulas Light"));

	}

	// @Test
	public void testEqualsToFunction() {
		given().get("https://jsonplaceholder.typicode.com/users").then().statusCode(200).body("address.id", equalTo(1))
				.log().all();
	}

	@Test
	public void testParametersandHeader() {
		given().param("Key", "Value").header("key", "value").get("https://jsonplaceholder.typicode.com/users").then()
				.body("address.street", hasItem("Kulas Light")).log().all();
	}

	@Test
	public void testCompleteText() {
		given().get("https://jsonplaceholder.typicode.com/users").then().body("address.street", hasItem("Kulas Light"));
	}

}
