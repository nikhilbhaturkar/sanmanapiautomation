package com.restapi.datalayer.pojo;

public class SinglePhoto {
	private int id;
	private int albumID;
	private String title;
	private String url;
	private String thumbUrl;

	public int getAlbumID() {
		return albumID;
	}

	public void setAlbumID(int albumID) {
		this.albumID = albumID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getThumbUrl() {
		return thumbUrl;
	}

	public void setThumbUrl(String thumbUrl) {
		this.thumbUrl = thumbUrl;
	}

	@Override
	public String toString() {
		return "SinglePhoto [id=" + id + ", albumID=" + albumID + ", title=" + title + ", url=" + url + ", thumbUrl="
				+ thumbUrl + "]";
	}

}
