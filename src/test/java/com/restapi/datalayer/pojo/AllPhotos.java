package com.restapi.datalayer.pojo;

import java.util.ArrayList;
import java.util.List;

public class AllPhotos {
	private List<SinglePhoto> allPhoto =new ArrayList<SinglePhoto>();

	public List<SinglePhoto> getAllPhoto() {
		return allPhoto;
	}

	public void setAllPhoto(List<SinglePhoto> allPhoto) {
		this.allPhoto = allPhoto;
	}
}
